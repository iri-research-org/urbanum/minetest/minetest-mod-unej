*English version below*

## Commandes de gestion de groupe 
#### `/u_grant <player_name|everyone|factions> <privs|privs_group|all>`

  Il s'agit d'une fonction `/grant <player> <privs|all>` augmentée, acceptant de donner un privs, un groupe de privs ou tous les privs à un joueur, une faction ou à tous les joueurs connectés. 
  - privs_group sont initialisés dans **/worldpath/privs_group.conf**.
  - les factions seront bientôt intégrées.

#### `/u_revoke <player_name|everyone|factions> <privs|privs_group|all>`

Il s'agit d'une fonction `/revoke <player> <privs|all>` augmentée, acceptant de révoquer un privs, un groupe de privs ou tous les privs d'un joueur, d'une faction ou de tous les joueurs connectés.
  - privs_group est une liste de chaînes et est initialisé dans /worldpath/privs_group.conf. 
  - les factions seront intégrées prochainement. factions will be integrate soon
`/u_empty_inventory <player|faction>`  
Vide l'inventaire d'un joueur ou d'une faction.

### `/u_give <player|faction> <identifier [\<amount\>[ \<wear\>]]|package>`

  Donne un objet ou un paquet à un joueur ou à une faction.
  C'est une extension de la commande de base `/give`
	<amount> : 1-65535, 1 par défaut est optionnel, n'affecte que les nœuds empilables, n'a pas d'effet autrement (par exemple, les outils).
	<wear> : 0-65535, la valeur par défaut est 0 (intact), la quantité doit être spécifiée, cela n'affecte que les outils.
	Fichier de configuration (`<world directory>/unej/manage_give.conf`) exemple :
```
      { "packages": 
        { 
          "my_package": [
            "default:brick 100",
            "default:pick_steel 1 30000"
          ] 
        }
      }
```

#### `/u_equip <player|faction> <identifier [\<amount\>[ \<wear\>]]|package>` 
  Remplace l'inventaire d'un joueur ou d'une faction.
  A les mêmes paramètres que `u_give`. 
  Remplace l'inventaire du joueur au lieu de le compléter. 

#### `/u_packages [\<package\>]`
   Liste et affiche des informations sur les paquets.
   Liste les paquets disponibles (si aucun argument n'est donné) ou le contenu du paquet désigné.

#### `unej.find_players(name)`

Retourne un tableau sous la forme {player_name = true} si le nom est un nom de joueur, ou avec plusieurs joueurs si le nom est un nom de faction, ou toutes les personnes connectées si le nom est « everyone ».
  Si un joueur a le même nom qu'une faction, vous pouvez ajouter « f_ » ou « p_ » pour une faction ou un joueur distinct.

#### `unej.find_privs(name)`

  Retourne une table sous la forme {privs_name = true} si name est un nom de privs, ou avec plusieurs privs si name est un groupe de privs (défini dans le fichier **privs_group.conf**), ou tous les privs si name == « all » !! « all » ne peut être utilisé que par unej_admin pour des raisons de sécurité.

#### `unej.manage_privs(name, player_name, grantprivstr, action)`

  Cette fonction est la fonction derrière `u_grant` et `u_revoke`
  - name est la personne qui a fait la commande de chat, elle sera notifiée en cas de succès de la commande
  - player_name est le joueur à qui l'on veut accorder ou révoquer des privs
  - grantprivstr est le privilège à changer
  - action est true pour l'octroi et nil pour la révocation

#### `unej.provision_player_names(name, query)`

  Retourne un tuple (liste de joueur, erreur).
  Bloquera à moins que les destinataires n'appartiennent à la faction possédée ou que l'appelant soit **unej_admin** ou **playerfactions_admin**.
  Retourne nil, error_msg dans ce cas.

## Commandes d’import/export de gros fichiers schematics
 - unej.max_lengths est la taille maximale des schematics créés par /worldcreate et /bigmtschemcreate. Augmenter la taille permet de créer moins de schémas, mais chaque schéma occupe plus d'espace.
	On peut considérer que 30 000 000 blocs ont besoin de 3Go pour être placés.
	La ligne doit être `unej.max_lengths = {« x »:number_value, « y »:number_value, « z »:number_value}`
  - unej.max_undo » est la taille du journal qui sera sauvegardé dans le undo world et le big placement. On peut considérer que chaque annulation nécessite 3Mo de stockage.

### `//bigmtschemcreate \<folder_name\>`

  Cette commande crée un dossier dans les schematics de la zone worldedit (qui doivent être déjà placés).
  C'est la même commande que `//mtschemcreate` sauf qu'elle permet de créer d'énormes schémas.
  ! ! Nécessite le mod worldedit

### `//bigmtschemplace \<folder_name\>`

  Cette commande importe un schematic créé par la commande //bigmtschemcreate.
  L'origine de l'importation est le pos1 de worldedit qui sera pris comme le coin inférieur gauche du schematic.
  C'est la même commande que `//mtschemplace` sauf qu'elle permet de placer d'énormes schematics.
  **! ! Nécessite le mod worldedit**

### `//worldjournal`

Il vous montre la liste des logs qui peuvent être annulés.

### `//worldundo [ID]`

  Annule une commande `//worldpap` ; `//worldplace` ou `//bigmtschemplace`.
  L'identifiant peut être donné par `//worldjournal`. 1 est l'action la plus récente.

### `unej.history_switch(cmd)`

  Mettre à jour **/worldpath/schems/history/history.dat** pour préparer la sauvegarde d'une nouvelle annulation (to undo cmd).

## Commandes d’import/export de monde avec protection et placement automatique selon les coordonnées géographique
Dans le dossier **unej.conf_path**
Nous pouvons choisir le chemin d'accès (conf_path) dans **init.lua**

**privs_group.conf ->**

    - teacher_privs est la liste des privilèges qui seront accordés lors de l'octroi des privilèges de l'enseignant avec la commande `/u_grant`
	- student_privs est la même chose mais pour les élèves
	- Nous pouvons ajouter d'autres groupes

**manage_privs.conf` ->**

    - Dans ce fichier, nous pouvons indiquer les privilèges qu'un joueur sera autorisé à gérer à l'aide des commandes `/u_privs` et `/u_revoke`. 
    - On peut mettre un nom de joueur ou un nom de personne comme clé. 
    - Si nous voulons interdire une permission à un joueur ou à un groupe de joueurs, nous pouvons ajouter un «  ! » avant le groupe de joueurs.
    - Exemple: "player_name":["interact","shout","fly"], "teacher":["!fly"] permettra au player_name de gérer interact, shout et fly, mais s'il a les privs de teacher, il ne pourra pas gérer fly 

### `//worldcreate \<folder_name\> [min_y] [max_y]`

Cette commande crée un dossier dans les schematics du monde

  Par défaut min_y = 20 et max_y = 148
  
  ! ! Cette commande nécessite le fichier **geometry.dat** situé sous **/worldpath/geometry.dat** lorsque le monde a été créé avec l'instance https://minecreaft.ign.fr
  
  Si ce fichier n'existe pas, la fonction renvoie une erreur

### `//worldplace \<param_y\> \<folder_name\>`

  Cette commande place les schematics créés par la fonction `//worldcreate`
  - param_y est l'altitude du fond où le schematic sera placé.
  La fonction sauvegardera sous /worldpath/placement.dat quelques données sur le placement :

```
           "nomplaque_altitude" : {
              "angleDegres" : -26.0,
          		"creation_time" : "2020/07/22 11:40:45",
          		"folder_name" : "nom_plaque",
          		"pos_max" : {
          			"x" : 60.0,
          			"y" : 3928.0,
          			"z" : 60.0
          		},
          		"pos_min" : {
          			"x" : -60.0,
          			"y" : 3800.0,
          			"z" : -60.0
          		},
          		"protected" : false,
          		"translation_X" : 655429.64208752569,
          		"translation_Z" : 6870118.3564836457
          	},
```

  ! ! Cette commande a besoin du fichier **geometry.dat** situé sous **/worldpath/geometry.dat** lorsque le monde a été créé avec l'instance https://minecraft.ign.fr

### `//worldpap \<param_y\> \<folder_name\> \<area_name\> [gap_y] [folder_name] [area_name] …`

 Cette fonction placera le world schematic exactement comme `//worldplace`. Ensuite, elle protégera le schéma importé :
  Elle va intégrer le schéma importé de **maptools:playerclip** [Et ça remplit le bas de **default:stone**. Le bas étant toutes les couches non uniquement constitué d'air, en partant du bas et remontant jusqu'à ce que la couche rencontrée ne contiennent aucun bloc d'air, s'il y a un problème avec ça on peut rajouter « false » après le premier area_name]
  Enfin, il crée une zone protégée avec vous-même en tant que propriétaire et le nom area_name.
  ! ! Nécessite les mods worldedit, areas et maptools

### `//protect \<area_name\>`

Cette commande permet de protéger une zone de worldedit :
Elle incorpore la zone de maptools:playerclip et crée une zone protégée, avec vous-même comme propriétaire et area_name.
  ! ! Nécessite les mods worldedit, areas et maptools

### `unej.open_file(path)`

Ouvrir et lire le fichier au format json

### `unej.write_file(path,data)`

 Inscrire les données du tableau dans le fichier

### `unej.set_vox_nodes(pos1,pos2,node_name)`

  Remplir rapidement la zone avec node_name

### `unej.table_unpack(data)`

Décompresse la table pour la transformer en une chaîne d'encodage json

### `unej.wmtscreate(player_name, params)`

  La fonction qui sous-tend `//worldcreate`.
  params = {<folder_name>, [min_y], [max_y]}

### `unej.wmtsplace(player_name,param_y,folder_name,cmd,new)`

  Place un monde et crée un fichier avec les données d'importation.
  C'est la seule fonction derrière `//worldplace`
  `cmd` est la commande qui a été faite par le joueur et qui ira dans le journal
  `new` est un bool caractérisant si vous voulez faire un nouveau undo ou si l'action sera sauvegardée avec le même undo que l'action précédente (utile dans le mode d'importation multiple `//worldpap`)

### `unej.protect(pos1,pos2,player_name,area_name,fill_in)` 

  Protéger une zone :
  - L'intégrer avec **maptools:playerclip**
  - Crée une zone protégée avec le nom du joueur et le nom de la zone.
  - **fill_in** est un boolean, true remplira le fond de la zone avec **default:stone**, couche par couche et en commençant par le bas, jusqu'à ce qu'une couche horizontale soit pleine de blocs (cela signifie qu'il n'y a plus d'air).
  On peut imaginer cela comme un verre que l'on remplit jusqu'à ce que le couvercle soit atteint.






*English version here*:


## Group management commands
### `/u_grant <player_name|everyone|factions> <privs|privs_group|all>`

This is an increased `/grant <player> <privs|all>` function, accepting to give a privs, a group of privs or all privs to a player, a faction or to every connected players.
  - privs_group are initialized in **/worldpath/privs_group.conf**
  - factions will be integrate soon

### `/u_revoke <player_name|everyone|factions> <privs|privs_group|all>`

  This is an increased `/revoke <player> <privs|all>` function, accepting to revoke a privs, a group of privs or all privs to a player, a faction or to every connected players.
  - privs_group is a list of string and is initialized in **/worldpath/privs_group.conf**
  - factions will be integrate soon

### `/u_empty_inventory <player|faction>`

  Empties inventory of a player or a faction.

### `/u_give <player|faction> <identifier [\<amount\>[ \<wear\>]]|package>`

  Gives an item or a package to a player or a faction.
  This is an extension of the basic `/give` command.
    <amount>: 1-65535, defaults to 1 is optional, only affects stackable nodes, has no effect otherwise (e.g. tools)
    <wear>: 0-65535, defaults to 0 (intact), requires amount to be specified, only affects tools.
    Configuration file **<world directory>/unej/manage_give.conf** example:

```
      { "packages": 
        { 
          "my_package": [
            "default:brick 100",
            "default:pick_steel 1 30000"
          ] 
        }
      }
```

### `/u_equip <player|faction> <identifier [\<amount\>[ \<wear\>]]|package>`

  Replaces inventory for a player or a faction.
  Have the same parameters than `u_give`.
  Replaces player inventory instead of adding to it

### `/u_packages [\<package\>]`

  List and displays information on packages.
  List available packages (if no argument is given) or the contents of designated package.

### `unej.find_players(name)`

  Return a table as {player_name = true} if name is a player name, or with many player if name is a faction name, or every connected people if name is "everyone".
  If a player has the same name as a faction, you can add "f_" or "p_" for separate faction or player.

### `unej.find_privs(name)`

  Return a table as {privs_name = true} if name is a privs name, or with many privs if name is a groupe of privs (define in the **privs_group.conf** file), or all privs if name == "all" !! "all" can only be used by **unej_admin** for safety.

### `unej.manage_privs(name, player_name, grantprivstr, action)`

  This function is the function behind `u_grant` and `u_revoke`
  - name is the person who made the chatcommand, he will be notify for success of the commands
  - player_name is the player to grant or revoke privs
  - grantprivstr is the privs to change
  - action is true for granting and nil to revoke

### `unej.provision_player_names(name, query)`

  Returns a tuple (list of player, error).
  Will block unless recipients belong to owned faction or caller is **unej_admin** or **playerfactions_admin**.
  Returns nil, error_msg in this case.

## Import/export commands for large schematics files 
- "unej.max_lengths" is the maximum size of schematics created by `/worldcreate` and `/bigmtschemcreate`. Increasing the size will make less schematics but each schemtaics will spend more space.
    You can consider that 30 000 000 blocs needs 3Go to be placed.
    The line needs to be `unej.max_lengths = {"x":number_value, "y":number_value, "z":number_value}`
  - `unej.max_undo` is the size of the journal that will be saved to undo world and big placement. You can consider that every undo needs 3Mo in storage.

### `//bigmtschemcreate \<folder_name\>`

  This command create a folder within schematics of the worldedit area (which need to be already placed).
  This is the same command as `//mtschemcreate` except it permit to create some huge schematics.
  **!! Needs the worldedit mod**

### `//bigmtschemplace \<folder_name\>`

  This command import a schematic created by the //bigmtschemcreate command.
  The origin of the importation is the worldedit pos1 which will be taken as the bottom left corner of the schematic.
  This is the same command as `//mtschemplace` except it permit to place some huge schematics.
  **!! Needs the worldedit mod**

### `//worldjournal`

  It shows you the list of log that can be undone

### `//worldundo [ID]`

  Undo a `//worldpap` ; `//worldplace` or `//bigmtschemplace` command.
  The id can be given by `//worldjournal`. 1 is the most recent action.

### `unej.history_switch(cmd)`

  Update **/worldpath/schems/history/history.dat** to prepare to a save a new undo (to undo cmd)

## World import/export commands with protection and automatic placement based on geographical coordinates
In **unej.conf_path** folder
We can choose the **conf_path** in **init.lua**

**privs_group.conf ->**

     - teacher_privs is the list of privs that will be granted when granting privs teacher with the command `/u_grant`
     - student_privs is the same but for students
     - We can add other groups

**manage_privs.conf ->**

### `//worldcreate \<folder_name\> [min_y] [max_y]`

  This command create a folder within schematics of the world.
  By default min_y = 20 and max_y = 148
  !! This command need the geometry.dat file located under /worldpath/geometry.dat when the world have been created with the  https://minecreaft.ign.fr instance.
  If this file doesn't exist the fonction will return an error.

### `//worldplace \<param_y\> \<folder_name\>`

  This command place schematics created by the `//worldcreate` function.
  - param_y is the altitude of the bottom of where the schematic will be placed.
  The function will save under **/worldpath/placement.dat** some data about the placement :

```
           "nomplaque_altitude" : {
              "angleDegres" : -26.0,
          		"creation_time" : "2020/07/22 11:40:45",
          		"folder_name" : "nom_plaque",
          		"pos_max" : {
          			"x" : 60.0,
          			"y" : 3928.0,
          			"z" : 60.0
          		},
          		"pos_min" : {
          			"x" : -60.0,
          			"y" : 3800.0,
          			"z" : -60.0
          		},
          		"protected" : false,
          		"translation_X" : 655429.64208752569,
          		"translation_Z" : 6870118.3564836457
          	},
```

  !! This command needs the geometry.dat file located under /worldpath/geometry.dat when the world have been created with the  https://minecraft.ign.fr instance.

### `//worldpap \<param_y\> \<folder_name\> \<area_name\> [gap_y] [folder_name] [area_name] …`

  This function will place the world schematic, exactly as `//worldplace`. Then it will protect the imported schematic:
  It will embed the imported schematic of **maptools:playerclip** [Et ça remplit le bas de **default:stone**. Le bas étant toutes les couches non uniquement constitué d'air, en partant du bas et remontant jusqu'à ce que la couche rencontrée ne contiennent aucun bloc d'air, si il y a un problème avec ça on peut rajouter "false" après le premier area_name]
  Finally it create a protected area with yourself as owner and the area_name.
  **!! Needs worldedit, areas and maptools mods**

### `//protect \<area_name\>`

  This command protect a worldedit area :
  It embed the area of **maptools:playerclip** and create an area protected, with yourself as owner and area_name.
  **!! Needs worldedit, areas and maptools mods**

### `unej.open_file(path)`

  Open and read the file as json

### `unej.write_file(path,data)`

  Write the table data in the file

### `unej.set_vox_nodes(pos1,pos2,node_name)`

  Fill quickly  the area with node_name

### `unej.table_unpack(data)`

  Unpack the table to transform in into a json encoding string.

### `unej.wmtscreate(player_name, params)`

  The function behinf `//worldcreate`.
  params = {<folder_name>, [min_y], [max_y]}

### `unej.wmtsplace(player_name,param_y,folder_name,cmd,new)`

  Place a world and create a file with the importation data.
  This is the only function behind `//worldplace`
  `cmd` is the command that have be made by player and will go into the journal
  `new` is a bool caractérising if you want to make a new undo or if the action will be saved with the same undo as the precedent action (usefull in the //worldpap multi import mode)

### `unej.protect(pos1,pos2,player_name,area_name,fill_in)`
 
  Protect an area :
  - Embed it with **maptools:playerclip**
  - Create a protected area with owner player_name and area_name.
  - fill_in is a boolean, true will fill the bottom of the area with **default:stone**, layer by layer and starting at the bottom, until an horizontal layer is full of blocks (it means there is no air).
  We can imagine as a glass that we fill until the lid is reached.
