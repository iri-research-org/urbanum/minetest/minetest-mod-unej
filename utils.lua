unej.table_unpack = function(data)
	if type(data) ~= "table" then
		return tostring(data)
	end
	local str = "{"
	local i = true
	for k,v in pairs(data) do
		if i then
			str = str .. "\""..k.."\":"..unej.table_unpack(v)
			i=false
		else
			str = str .. ", \""..k.."\":"..unej.table_unpack(v)
		end
	end
	str = str .. "}"
	return str
end

unej.open_file = function(path, warn)
	warn = (warn==nil) or warn
	local file = io.open(path,"r")
	if not file then
		if warn then
			minetest.log("warning","UNEJ mod -!- failed opening " .. path .. ".")
		end
		return false
	end
	local content = file:read("*all")
	file:close()
	local data = minetest.parse_json(content)
	if not data then
		minetest.log("error","UNEJ mod -!- Failed to convert json to table " .. path .. "\nCheck that this file is a json file.")
		return false
	end
	return data
end

--  follow keys in nested subtables of json data
unej.table_nested_value = function(data, ...)
	for _, key in ipairs{...} do data = (data or {})[key] end
	return data
end

unej.write_file = function(path, data)
	local file = io.open(path,"w+")
	if not file then
		return false
	end
	file:write(minetest.write_json(data,true))
	file:close()
	return true
end

unej.set_vox_nodes = function(pos1,pos2,node_name)
	local id = minetest.get_content_id(node_name)
	local manip = minetest.get_voxel_manip()
	local emerged_pos1, emerged_pos2 = manip:read_from_map(pos1, pos2)
	local area = VoxelArea:new({MinEdge=emerged_pos1, MaxEdge=emerged_pos2})
	local data = {}
	local c_ignore = minetest.get_content_id("ignore")
	for i = 1, worldedit.volume(area.MinEdge, area.MaxEdge) do
		data[i] = c_ignore
	end
	for i in area:iterp(pos1, pos2) do
		data[i] = id
	end
	manip:set_data(data)
	manip:write_to_map()
end

unej.history_switch = function(cmd)
	local last = #unej.history
	if last >= unej.history_max_undo then
		table.insert(unej.history,1,{folder_name=unej.history[last].folder_name, commande=cmd, pos={}})
		table.remove(unej.history,last+1)
	else
		table.insert(unej.history,1,{folder_name=""..last+1, commande=cmd, pos={}})
	end
end

unej.protect = function(pos1,pos2,player_name,area_name,fill_in)
	-- Entourage de la plaque de bloc transparents
	minetest.chat_send_player(player_name,"Enrobage de playerclip en cours...")

	unej.set_vox_nodes(pos1, {x=pos1.x,y=pos2.y,z=pos2.z},"maptools:playerclip")
	unej.set_vox_nodes(pos1, {x=pos2.x,y=pos2.y,z=pos1.z},"maptools:playerclip")
	unej.set_vox_nodes({x=pos2.x,y=pos1.y,z=pos1.z}, pos2,"maptools:playerclip")
	unej.set_vox_nodes({x=pos1.x,y=pos1.y,z=pos2.z}, pos2,"maptools:playerclip")
	unej.set_vox_nodes({x=pos1.x,y=pos2.y,z=pos1.z}, {x=pos2.x,y=pos2.y,z=pos2.z},"maptools:playerclip")

	local y_min = pos1.y
	if fill_in then
		for iter_y = pos1.y,pos2.y do
			local pos3 = {x=pos1.x+1,y=iter_y,z=pos1.z+1}
			local pos4 = {x=pos2.x-1,y=iter_y,z=pos2.z-1}
			local manip = minetest.get_voxel_manip()
			local emerged_pos3, emerged_pos4 = manip:read_from_map(pos3, pos4)
			local area = VoxelArea:new({MinEdge=emerged_pos3, MaxEdge=emerged_pos4})
			local data = manip:get_data()
			air_id = minetest.get_content_id("air")
			only_air = true
			no_air = true
			for  i in area:iterp(pos3, pos4) do
				if data[i] ~= air_id then
				only_air = false
				else
					no_air = false
				end
			end
			if not only_air then
				if y_min == pos1.y then
					y_min = iter_y
				else
					unej.set_vox_nodes(pos3,pos4,"default:stone")
				end
			end
			if no_air then
				break
			end
		end
	end
	unej.set_vox_nodes({x=pos1.x,y=y_min,z=pos1.z}, {x=pos2.x,y=y_min,z=pos2.z},"maptools:playerclip")


	minetest.chat_send_player(player_name,"Enrobage terminé")
	minetest.chat_send_player(player_name,"Protection de la zone en cours...")
	-- Protection de l'intégralité de ce qui a été créé
	local id = areas:add(player_name, area_name, pos1, pos2, nil)
	areas:save()
	unej.history[1].area = id
	unej.write_file(minetest.get_worldpath() .. "/schems/history/history.dat",unej.history)
	minetest.chat_send_player(player_name,"Area protected. ID: @"..id)
	return id
end

unej.remove_dangling_privs = function(player, _)
	local name = player:get_player_name()
	local player_privs, modified, summary = minetest.get_player_privs(name), false, ""
	for priv_name, _ in pairs(player_privs) do
		if (string.match (priv_name, "u_give_")
		or string.match (priv_name, "u_grant_"))
		and not minetest.registered_privileges[priv_name] then
			player_privs[priv_name] = nil
			modified = true
			summary = summary .. " " .. priv_name
		end
	end
	if modified then
		minetest.set_player_privs(name, player_privs)
		minetest.chat_send_player(name, "Privilege(s) " .. summary .. " removed since out-of-date")
	end
end

-- Will block unless recipients belong to owned faction or caller is unej_admin or playerfactions_admin
unej.provision_player_names = function(name, query)
	if minetest.player_exists(query) then
		if name == query or minetest.check_player_privs(name, "playerfactions_admin") then
			return { [query] = true }
		end
		for _, faction in pairs(factions.get_administered_factions(name) or {}) do
			if factions.player_is_in_faction(faction, query) then return { [query] = true } end
		end
		return nil, query.." is not a member of any faction you own."
	end
	local facts = factions.get_facts()
	if not facts then return nil, "Could not find player or faction named "..query..", no factions" end
	local query_faction = facts[query]
	if not query_faction then return nil, "Could not find player or faction named "..query end
	if minetest.check_player_privs(name, "playerfactions_admin") or query_faction.owner == name then
		return query_faction.members
	end
	return nil, "You do not own faction "..query.."."
end

-- Returns true, extra parameters on success or false, reason on failure
unej.process_names = function(name, params, command_name)
	local targets, next_params = string.match(params, "([%S]+)[%s]*(.*)")
	if not targets then return false,  "Invalid parameters (see /help "..command_name..")" end
	local player_names, reason = unej.provision_player_names(name, targets)
	if not player_names then return false, reason end
	local and_me, extra_params = string.match(next_params, "(and_me)[%s]*(.*)")
	if and_me then
		next_params = extra_params
	elseif targets ~= name then
		player_names[name] = nil
	end
	return player_names, next_params
end

unej.register_outcome = function(names_by_outcome, name, outcome)
	if names_by_outcome[outcome] then
		table.insert(names_by_outcome[outcome], name)
	else
		names_by_outcome[outcome] = { name }
	end
end

local chat_line_length_threshold = 100

unej.summarize_iteration = function (names_by_outcome)
	local summary = "Effect on players :\n"
	for outcome, names_list in pairs(names_by_outcome) do
		local line = outcome.." ("..tostring(#names_list).."):"
		local omitted_players = 0
		for _, name in ipairs(names_list) do
			if string.len(line) < chat_line_length_threshold then
				line = line.." "..name
			else
				omitted_players = omitted_players + 1
			end
		end
		if omitted_players > 0 then
			line = line.." and "..tostring(omitted_players).." more."
		end
		summary = summary..line.."\n"
	end
	return summary
end
