minetest.register_chatcommand("/worldundo", {
	params = "<id>",
	description = "Undo a world placement",
	privs = {unej_admin=true},
	func = function(name, param)
	  local history = nil
	  if param and tonumber(param) <= #unej.history then
			history = unej.history[tonumber(param)]
	  else
	    minetest.chat_send_player(name,"Error with parameter")
	    return false
	  end
	  minetest.chat_send_player(name,"Begin to cancel")
	  local path = minetest.get_worldpath() .. "/schems/history/" .. history.folder_name.."/"
		for i = #history.pos, 1,-1 do
	    if not minetest.place_schematic(history.pos[i], path..i..".mts") then
	      minetest.chat_send_player(name,"The undo failed")
	    end
	  end
	  if history.area then
	    areas:remove(history.area, true)
	    areas:save()
	  end
	  minetest.chat_send_player(name,"The undo succeed")
	end})

minetest.register_chatcommand("/worldjournal", {
	params = "",
	description = "List logs that can be undone",
	privs = {unej_admin=true},
	func = function(name)
		local list = "Voici les logs des placements de monde que vous pouvez undo (avec leur id d'undo)\n"
		for i,v in ipairs(unej.history) do
			list = list .. " ["..i.."] "..v.commande.." ;\n"
		end
		minetest.chat_send_player(name,list)
	end})
