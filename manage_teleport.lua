local ground_search_range = 10
local ground_search_tolerance = 10
local airborne_search_tolerance = 10
local object_exclusion_range = 0.8

local function node_name(x, y, z)
	return minetest.get_node({ x = x, y = y , z = z}).name
end

-- Destination should not be obstructed
local function air_here_and_above(x, y, z)
	return node_name(x, y, z) == "air" and node_name(x, y + 1, z) == "air"
end
-- Find object-free ground surface above or below given coordinates and within vertical range
local function find_ground(x, y, z)
	local y_offset = 0
	while air_here_and_above(x, y + y_offset, z) do
		if y_offset + ground_search_range < 0 then return y, false end
		y_offset = y_offset - 1
	end
	while not air_here_and_above(x, y + y_offset, z) do
		if y_offset > ground_search_range then return y, false end
		y_offset = y_offset + 1
	end
	if next(minetest.get_objects_inside_radius({ x = x, y = y + y_offset, z = z}, object_exclusion_range)) or
		not (minetest.registered_nodes[node_name(x, y + y_offset - 1, z)] or {})["walkable"] then
		return y, false
	end
	return y + y_offset, true
end

local function find_air(x, y, z)
	local y_offset = 0
	while not air_here_and_above(x, y + y_offset, z) do
		if y_offset > ground_search_range then return y, false end
		y_offset = y_offset + 1
	end
	if next(minetest.get_objects_inside_radius({ x = x, y = y + y_offset, z = z}, object_exclusion_range)) then
		return y, false
	end
	return y + y_offset, true
end


-- Iterating this generates a spiral pattern around the origin
local function next_x_z_offset(x, z)
	if x + z > 0 then
		if z > x then
			return x + 1, z
		else
			return x, z - 1
		end
	elseif z < x then
		return x - 1, z
	else
		return x, z + 1
	end
end

-- Find ground along spiral. Score provides a heuristic threshold to give up on bad coordinates.
local function provision_destinations(x, y, z, on_ground, list)
	local x_offset, z_offset, found = 0, 0, false
	local score = on_ground and ground_search_tolerance or airborne_search_tolerance
	for name, _ in pairs(list) do
		while not found do
			if score < 0 then return nil end
			if on_ground then
				y, found = find_ground(x + x_offset, y, z + z_offset)
			else
				y, found = find_air(x + x_offset, y, z + z_offset)
			end
			if found then
				score = score < ground_search_tolerance and ground_search_tolerance or score + 1
				list[name] = { x = x + x_offset, y = y, z = z + z_offset }
			else
				score = score - 1
			end
			x_offset, z_offset = next_x_z_offset(x_offset, z_offset)
		end
		found = false
	end
	return list
end

local function teleport_players(dest_list)
	local names_by_outcome, player, outcome = {}
	for player_name, dest in pairs(dest_list) do
		player = minetest.get_player_by_name(player_name)
		if player then
			player:set_pos(dest)
			outcome = "teleported"
		else
			outcome = "absent"
		end
		unej.register_outcome(names_by_outcome, player_name, outcome)
	end
	return true, unej.summarize_iteration(names_by_outcome)
end

minetest.register_chatcommand("u_teleport", {
	params = "<player|faction> [<target_player|x,y,z>] [-g]\
	-g: cancel teleportation unless destination is on safe ground",
	description = "Teleport a player or faction to given coordinates or player.\
	If no destination is given, then destination is your position",
	privs = { u_teleport = true },
	func = function(name, params)
		local target, next_params = string.match(params, "(%S+)%s*(.*)")
		if not target then return false,  "Invalid parameters (see /help u_teleport)" end
		local names_list, reason = unej.provision_player_names(name, target)
		if not names_list then return false, reason end
		local destination, ground = string.match(next_params, "(.*)(-g)")
		if not ground then destination = next_params end
		if destination == "" then
			target = minetest.get_player_by_name(name):get_pos()
		else
			local x_str, y_str, z_str = string.match(destination, "(%-?%d+),(%-?%d+),(%-?[%d]+)")
			if x_str then
				target = { x = tonumber(x_str), y = tonumber(y_str), z = tonumber(z_str) }
			else
				local target_player = minetest.get_player_by_name(destination)
				if target_player then
					target = target_player:get_pos()
				else
					return false, "Invalid destination."
				end
			end
		end
		local dest_list = provision_destinations(target.x, target.y, target.z, true, names_list)
		if dest_list then
			return teleport_players(dest_list)
		elseif ground then
			return false, "No ground destination found."
		else
			dest_list = provision_destinations(target.x, target.y, target.z, false, names_list)
			if dest_list then return teleport_players(dest_list) end
			return false, "Could not find destination."
		end
	end
})


minetest.register_privilege("u_teleport", {description = "allows u_teleport", give_to_singleplayer = false})
